defmodule Auth.Model do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user" do
    field(:username, :string)
    field(:password, :string)

    timestamps()
  end

  def changeset(user, params \\ %{}) do
    user
    |> cast(params, [:username, :password])
    |> validate_required([:username, :password])
    |> unique_constraint(:username)
    |> validate_length(:username, min: 3)
    |> validate_length(:password, min: 8)
  end
end
