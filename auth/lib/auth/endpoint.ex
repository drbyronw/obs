defmodule Auth.Endpoint do
  use Plug.Router
  alias Auth.Controller

  plug(Plug.Logger)
  plug(:match)
  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)
  plug(:dispatch)

  post "register" do
    {status, body} =
      case conn.body_params do
        %{"username" => username, "password" => password} ->
          registeredUser = Controller.register(username, password)

          case registeredUser do
            {:ok, %Auth.Model{id: id, username: username}} ->
              {200,
               Poison.encode!(%{
                 "success" => true,
                 "token" =>
                   Auth.Token.generate_and_sign!(%{"user_id" => id, "username" => username})
               })}

            {:error, reason} ->
              {404, Poison.encode!(%{"success" => false, "reason" => reason})}
          end

        _ ->
          {404, Poison.encode!(%{"error" => "error lol"})}
      end

    send_resp(conn, status, body)
  end

  post "login" do
    {status, body} =
      case conn.body_params do
        %{"username" => username, "password" => password} ->
          u = Controller.login(username, password)

          case u do
            {:ok, token} -> {200, Poison.encode!(%{"success" => true, "token" => token})}
            {:error, reason} -> {404, Poison.encode!(%{"success" => false, "reason" => reason})}
          end

        _ ->
          {404, Poison.encode!(%{"error" => "error lol"})}
      end

    send_resp(conn, status, body)
  end
end
