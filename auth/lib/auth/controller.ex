defmodule Auth.Controller do
  alias Auth.Model

  def register(username, password) do
    IO.puts("\n\n\n" <> password)
    IO.puts("\n\n\n" <> username)

    cs =
      %Model{}
      |> Model.changeset(%{username: username, password: password})
      |> Auth.Repo.insert()

    case cs do
      {:ok, cs} ->
        {:ok, cs}

      {:error, cs} ->
        {:error, changeset_error_to_string(cs)}
    end
  end

  def changeset_error_to_string(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
    |> Enum.reduce("", fn {k, v}, acc ->
      joined_errors = Enum.join(v, "; ")
      "#{acc}#{k}: #{joined_errors}\n"
    end)
  end

  def login(username, password) do
    result = Auth.Repo.get_by(Model, username: username, password: password)

    case result do
      %Model{id: id, username: username} ->
        extra_claims = %{"user_id" => id, "username" => username}
        token_with_default_plus_custom_claims = Auth.Token.generate_and_sign!(extra_claims)
        {:ok, token_with_default_plus_custom_claims}

      _ ->
        {:error, "user not found or incorrect password"}
    end
  end
end
