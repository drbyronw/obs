defmodule Auth.Repo.Migrations.UniqueUsername do
  use Ecto.Migration

  def change do
    create unique_index(:user, [:username])
  end
end
