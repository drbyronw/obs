import Config

config :auth, Auth.Repo,
  database: "auth_repo",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool_size: 10

config :logger, level: :warn

config :auth, ecto_repos: [Auth.Repo]

config :joken, default_signer: "secret"

import_config "#{Mix.env()}.exs"
