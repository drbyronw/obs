# Given When Then Scenarios

**Scenario**: New client Bob wants to register for OBS
  - **Given**: Bob is new and doesn’t have an OBS account
  - **When**: Bob opens the OBS homepage and he inputs the necessary data to sign-up
  - **Then**: Bob will see a registration confirmation and he’s then logged into to the system where he sees the dashboard first and can make transactions whenever

**Scenario**: Authenticated user Bob is registered for OBS and wants to view his dashboard
  - **Given**: Bob has already registered for an OBS account
  - **When**: Bob signs into his account using his authenticated credentials in the homepage sign-in
  - **Then**: Bob will see his dashboard and options to make transactions

**Scenario**: Un-authenticated user Bob is trying to engage in stock transactions
  - **Given**: Bob has not registered for an official OBS account
  - **When**: Bob opens the OBS homepage and inputs necessary information to make a stock transaction
  - **Then**: Bob is given an error message and he cannot make any stock transactions

**Scenario**: OBS dashboard displays current stock prices for Bank Inc stocks with client portfolio information.
  - **Given**: Bob has an OBS account
  - **When**: Bob opens the “Dashboard” page in a web browser
  - **Then**: Bob sees all five current stock prices AND their portfolio information (current # of shares of all five stocks and amount of money in account)

**Scenario**: Clients must input valid username/password combination or given an error message when logging in.
  - **Given**: Bob has registered for access AND has an OBS account
  - **When**: Bob opens the “Login” page in a web browser AND incorrectly enters the required fields
  - **Then**: Bob sees a login failed response AND is not logged in to the system - redirected to “Login” page

**Scenario**: Authed user cannot purchase shares at a cost greater than the cash held in the account
  - **Given**: Bob has logged in AND attempts to purchase more shares than he can currently afford
  - **When**: Bob opens the “Transaction” page in a web browser AND properly enters to buy an amount of shares with cost greater than held in the account
  - **Then**: Bob sees a transaction failed response AND redirected back to the “Transaction” page

**Scenario**: Bob wants to create another account to buy different stocks
  - **Given**: Bob already has an OBS account registered
  - **When**: Bob opens the “Registration” page in a web browser AND properly enters the required fields
  - **Then**: Bob sees a registration confirmation AND is logged in to the system AND is redirected to the dashboard AND database is updated with another account for the same user_id

**Scenario**: Bob wants to add funds to his account to buy more stocks
  - **Given**: Bob already has an OBS account registered AND is signed in
  - **When**: Bob opens the “add funds” page AND enters a valid number
  - **Then**: Bob is redirected to the dashboard AND sees a confirmation that funds have been added AND the OBS dashboard should be updated with the correct amount

**Scenario**: Bob wants to see his holdings (stock and funds) per account
  - **Given**: Bob is an authorized user AND already has multiple OBS accounts registered AND has purchased stocks or added funds to more than one account
  - **When**: Bob opens the dashboard page in a web browser AND looks at a list of his assets
  - **Then**: Bob sees his stock and cash holdings divided between his various accounts corresponding to which account was used to purchase/add funds

**Scenario**: Logged in user Bob wants to purchase or sell shares
  - **Given**: Bob is authorized AND has shares or funds to use for transactions
  - **When**: Bob opens the buy/sell page in a web browser AND enters the amount of his desired transaction (either how many shares to sell or how many to buy) AND has adequate assets (either enough shares to sell the desired amount or enough money to purchase desired number of shares) AND submits the buy/sell form
  - **Then**: Bob sees a transaction confirmation AND Bob’s assets have changed accordingly (either acquiring new shares and losing funds or acquiring more funds and losing shares)

**Scenario**: Logged in user Bob wants to open more than 3 accounts
  - **Given**: Bob is an authorized user AND has 3 existing accounts
  - **When**: Bob opens up the Create Account page in a web browser AND submits the form to open an additional account
  - **Then**: Bob sees an error message on the Create Account page that indicates he has opened the maximum number of accounts