use Mix.Config

config :kraft_heinz, Repo,
  database: "kraft_heinz",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_USER") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost"

config :kraft_heinz, Kraft_Heinz, API_TOKEN: System.get_env("API_TOKEN")

config :kraft_heinz, ecto_repos: [Repo]

config :joken, default_signer: "secret"

config :logger, level: :info

if Mix.env() == :test do
  import_config "#{Mix.env()}.exs"
end
