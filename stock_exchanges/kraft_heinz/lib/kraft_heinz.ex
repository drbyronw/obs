defmodule Kraft_Heinz do
  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repositor
      Repo,
      Plug.Adapters.Cowboy.child_spec(
        scheme: :http,
        plug: Router,
        options: [port: System.get_env("PORT") || 4002]
      )
    ]

    opts = [strategy: :one_for_one]

    {:ok, pid} = Supervisor.start_link(children, opts)

    case Functions.get_bank_shares() do
      0 ->
        header = [
          Authorization: "Bearer #{System.get_env("API_TOKEN")}",
          Accept: "application/json"
        ]

        {:ok, %HTTPoison.Response{body: body}} =
          HTTPoison.get("https://sandbox.tradier.com/v1/markets/quotes?symbols=KHC", header)

        decoded_body = Jason.decode!(body)

        %{
          "quotes" => %{
            "quote" => %{
              "last" => last
            }
          }
        } = decoded_body

        total_cost = 5000 * last

        Repo.insert(%Ledger{type: "buy", shares: 5000, quoted_price: last, total_cost: total_cost})

        Repo.insert(%Logs{type: "buy", user_id: 0, shares: 5000, quoted_price: last})
        {:ok, pid}

      _ ->
        {:ok, pid}
    end
  end
end
