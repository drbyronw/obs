defmodule Repo do
  use Ecto.Repo,
    otp_app: :kraft_heinz,
    adapter: Ecto.Adapters.Postgres
end
