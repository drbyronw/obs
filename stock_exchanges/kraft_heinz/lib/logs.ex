defmodule Logs do
  use Ecto.Schema

  schema "logs" do
    field(:type, :string)
    field(:user_id, :integer)
    field(:shares, :integer)
    field(:quoted_price, :float)

    timestamps()
  end
end
