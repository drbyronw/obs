defmodule Repo do
  use Ecto.Repo,
    otp_app: :lyft,
    adapter: Ecto.Adapters.Postgres
end
