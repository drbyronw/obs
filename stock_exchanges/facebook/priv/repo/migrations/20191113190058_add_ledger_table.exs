defmodule Repo.Migrations.AddLedgerTable do
  use Ecto.Migration

  def change do
    create table (:ledger) do
      add :type, :string
      add :shares, :integer
      add :quoted_price, :float
      add :total_cost, :float
      add :symbol, :string

      timestamps()
    end
  end
end
