# Unit tests
defmodule FunctionsTest do
  use ExUnit.Case
  doctest Functions

  import Ecto.Query
  import ExMock

  @transaction1 %{
    shares: 10,
    id: nil,
    inserted_at: nil,
    quoted_price: 25.0,
    type: "buy",
    updated_at: nil,
    user_id: nil
  }

  @transaction2 %{
    shares: 30,
    id: nil,
    inserted_at: nil,
    quoted_price: 10.0,
    type: "sell",
    updated_at: nil,
    user_id: nil
  }

  @transaction3 %{
    shares: 40,
    id: nil,
    inserted_at: nil,
    quoted_price: 5.0,
    type: "buy",
    updated_at: nil,
    user_id: nil
  }

  @emptyTransactionLog []
  @oneItemTransactionLog [@transaction1]
  @multiItemTransactionLog1 [@transaction3, @transaction2]
  @multiItemTransactionLog2 [@transaction1, @transaction1, @transaction1, @transaction2]

  # profit_loss helper function tests

  test "profit_loss returns a number" do
    assert is_number(Functions.profit_loss(@oneItemTransactionLog))
  end

  test "profit_loss empty list" do
    assert Functions.profit_loss(@emptyTransactionLog) == 0
  end

  # Check if this is the expected behavior
  test "profit_loss one item list (loss)" do
    assert Functions.profit_loss(@oneItemTransactionLog) == -250.0
  end

  # This should be negative?
  test "profit_loss multi-item list (loss)" do
    assert Functions.profit_loss(@multiItemTransactionLog2) == -450.0
  end

  # This is the loss on the 30 shares that were sold plus the value of the remaining 10 shares at 5
  test "profit_loss multi-item list (profit)" do
    assert Functions.profit_loss(@multiItemTransactionLog1) == 100.0
  end

  # get_shares helper function tests

  test "get_shares returns a number" do
    assert is_number(Functions.get_shares(@oneItemTransactionLog))
  end

  test "get_shares empty list" do
    assert Functions.get_shares(@emptyTransactionLog) == 0
  end

  test "get_shares one item list (net-gain)" do
    assert Functions.get_shares(@oneItemTransactionLog) == 10.0
  end

  test "get_shares multi-item list" do
    assert Functions.get_shares(@multiItemTransactionLog1) == 10.0
  end

  test "get_shares multi-item list (zero)" do
    assert Functions.get_shares(@multiItemTransactionLog2) == 0.0
  end

  # Additional Function tests

  test "test profit loss function" do
    list =
      Enum.map([1, 2, 3], fn x ->
        %Logs{
          quoted_price: x * 90.56,
          shares: x * 100,
          type: "buy"
        }
      end)

    assert Functions.profit_loss(list) == -126_784.0
  end

  test "test get_shares function" do
    list =
      Enum.map([1, 2, 3], fn x ->
        %Logs{
          quoted_price: x * 90.56,
          shares: x * 100,
          type: "buy"
        }
      end)

    assert Functions.get_shares(list) == 600
  end

  # get_bank_shares function tests

  # @tag :db
  # test "split controller should attempt to insert into database with correct values" do
  #   with_mock Repo, all: fn query -> [] end do

  #     Functions.get_bank_shares("FB")
  #     assert called(Repo.all( from t in Ledger, where: t.symbol == "FB", select: t))
  #   end
  # end
end
