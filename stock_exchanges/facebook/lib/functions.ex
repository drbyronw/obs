defmodule Functions do
  import Ecto.Query

  def profit_loss(transactions) when is_list(transactions) do
    Enum.reduce(transactions, 0, fn transaction, acc ->
      case transaction.type do
        "buy" ->
          acc - transaction.shares * transaction.quoted_price

        "sell" ->
          acc + transaction.shares * transaction.quoted_price

        _ ->
          acc
      end
    end)
  end

  def get_shares(transactions) when is_list(transactions) do
    Enum.reduce(transactions, 0, fn transaction, acc ->
      case transaction.type do
        "buy" ->
          acc + transaction.shares

        "sell" ->
          acc - transaction.shares

        _ ->
          acc
      end
    end)
  end

  def get_bank_shares(symbol) do
    query = from(t in Ledger, where: t.symbol == ^symbol, select: t)
    logs = Repo.all(query)

    get_shares(logs)
  end
end
