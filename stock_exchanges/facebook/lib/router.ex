defmodule Router do
  use Plug.Router
  import Plug.Conn
  import Ecto.Query

  plug(:match)
  plug(Plug.Parsers, parsers: [:urlencoded])
  plug(:dispatch)

  @header [Authorization: "Bearer #{System.get_env("API_TOKEN")}", Accept: "application/json"]

  get "/price/:symbol/" do
    map = Enum.into(conn.req_headers, %{}, fn a -> a end)
    IO.inspect(map)
    %{"token" => token} = map

    case Token.verify_and_validate(token) do
      {:ok, claims} ->
        {:ok, %HTTPoison.Response{status_code: status_code, body: body}} =
          HTTPoison.get(
            "https://sandbox.tradier.com/v1/markets/quotes?symbols=#{symbol}",
            @header
          )

        case status_code do
          401 ->
            conn
            |> send_resp(404, Poison.encode!(%{Response: "Failed. Incorrect API KEY!"}))

          _ ->
            decoded_body = Jason.decode!(body)

            %{
              "quotes" => %{
                "quote" => %{
                  "ask" => ask,
                  "change" => change,
                  "high" => high,
                  "last" => last,
                  "low" => low
                }
              }
            } = decoded_body

            Repo.insert(%Logs{type: "price", user_id: 0, quoted_price: last})

            conn
            |> send_resp(
              200,
              Jason.encode!(%{
                Response: "Success",
                ask: ask,
                change: change,
                high: high,
                last: last,
                low: low
              })
            )
        end

      {:error, reason} ->
        conn
        |> send_resp(
          404,
          Poison.encode!(%{Response: "Failed. Incorrect user token KEY! #{reason}"})
        )
    end
  end

  get "/logs" do
    map = Enum.into(conn.req_headers, %{}, fn a -> a end)
    %{"token" => token} = map

    case Token.verify_and_validate(token) do
      {:ok, claims} ->
        logs = Repo.all(Logs)

        Repo.insert(%Logs{type: "logs", user_id: 0})

        case logs do
          [] ->
            conn
            |> send_resp(404, Poison.encode!(%{Response: "Failed. No Logs for user"}))

          _ ->
            conn
            |> send_resp(200, Poison.encode!(logs))
        end

      {:error, reason} ->
        conn
        |> send_resp(
          404,
          Poison.encode!(%{Response: "Failed. Incorrect user token KEY! #{reason}"})
        )
        |> halt
    end
  end

  get "/pl" do
    map = Enum.into(conn.req_headers, %{}, fn a -> a end)
    %{"token" => token, "symbol" => symbol} = map

    case Token.verify_and_validate(token) do
      {:ok, claims} ->
        logs = Repo.all(Ledger)

        case logs do
          [] ->
            conn
            |> send_resp(404, Poison.encode!(%{Response: "Failed. No Transactions in Ledger"}))

          _ ->
            pl = Float.round(Functions.profit_loss(logs), 2)
            holdings = Functions.get_bank_shares(symbol)

            {:ok, %HTTPoison.Response{status_code: status_code, body: body}} =
              HTTPoison.get(
                "https://sandbox.tradier.com/v1/markets/quotes?symbols=#{symbol}",
                @header
              )

            case status_code do
              401 ->
                conn
                |> send_resp(404, Poison.encode!(%{Response: "Failed. Incorrect API KEY!"}))

              _ ->
                decoded_body = Jason.decode!(body)

                %{
                  "quotes" => %{
                    "quote" => %{
                      "ask" => ask,
                      "change" => change,
                      "high" => high,
                      "last" => last,
                      "low" => low
                    }
                  }
                } = decoded_body

                Repo.insert(%Logs{type: "price", user_id: 0, quoted_price: last})
                Repo.insert(%Logs{type: "profit-loss", user_id: 0})

                pl = Float.round(pl + last * holdings, 2)

                conn
                |> send_resp(200, Poison.encode!(%{Response: "Success", PL: pl}))
            end
        end

      {:error, reason} ->
        conn
        |> send_resp(
          404,
          Poison.encode!(%{
            Response: "Error",
            Reason: "Failed. Incorrect user token KEY! #{reason}"
          })
        )
        |> halt()
    end
  end

  get "/holdings" do
    map = Enum.into(conn.req_headers, %{}, fn a -> a end)
    %{"token" => token} = map

    case Token.verify_and_validate(token) do
      {:ok, claims} ->
        logs = Repo.all(Ledger)

        case logs do
          [] ->
            conn
            |> send_resp(404, Poison.encode!(%{Response: "Failed. No Logs for user"}))
            |> halt

          _ ->
            shares_owned = Functions.get_shares(logs)

            Repo.insert(%Logs{type: "shares", user_id: 0})

            conn
            |> send_resp(200, Poison.encode!(%{Response: "Success", shares_owned: shares_owned}))
        end

      {:error, reason} ->
        conn
        |> send_resp(
          404,
          Poison.encode!(%{Response: "Failed. Incorrect user token KEY! #{reason}"})
        )
        |> halt
    end
  end

  post "/buy" do
    {:ok, _body, conn} = Plug.Conn.read_body(conn)
    %{"shares" => shares, "token" => token, "symbol" => symbol} = conn.body_params

    case Token.verify_and_validate(token) do
      {:ok, claims} ->
        %{"user_id" => user_id} = claims

        {:ok, %HTTPoison.Response{status_code: status_code, body: body}} =
          HTTPoison.get(
            "https://sandbox.tradier.com/v1/markets/quotes?symbols=#{symbol}",
            @header
          )

        case status_code do
          401 ->
            conn
            |> send_resp(404, Poison.encode!(%{Response: "Failed. Incorrect API KEY!"}))

          _ ->
            decoded_body = Jason.decode!(body)

            %{
              "quotes" => %{
                "quote" => %{
                  "last" => last
                }
              }
            } = decoded_body

            {shares, _} = Integer.parse(shares)

            bank_shares = Functions.get_bank_shares(symbol)
            total_cost = 5000 * last

            cond do
              bank_shares <= shares ->
                number_to_buy = 5000 + (shares - bank_shares)

                Repo.insert(%Ledger{
                  type: "buy",
                  shares: number_to_buy,
                  quoted_price: last,
                  total_cost: total_cost,
                  symbol: symbol
                })

                Repo.insert(%Logs{
                  type: "buy",
                  user_id: 0,
                  shares: number_to_buy,
                  quoted_price: last,
                  symbol: symbol
                })

              true ->
                nil
            end

            Repo.insert(%Logs{
              type: "buy",
              user_id: user_id,
              shares: shares,
              quoted_price: last,
              symbol: symbol
            })

            Repo.insert(%Logs{
              type: "sell",
              user_id: 0,
              shares: shares,
              quoted_price: last,
              symbol: symbol
            })

            total_cost = shares * last

            Repo.insert(%Ledger{
              type: "sell",
              shares: shares,
              quoted_price: last,
              total_cost: total_cost,
              symbol: symbol
            })

            conn
            |> send_resp(
              200,
              Jason.encode!(%{user_id: user_id, shares: shares, quoted_price: last})
            )
        end

      {:error, reason} ->
        conn
        |> send_resp(
          404,
          Poison.encode!(%{Response: "Failed. Incorrect user token KEY! #{reason}"})
        )
    end
  end

  post "/sell" do
    {:ok, _body, conn} = Plug.Conn.read_body(conn)
    %{"token" => token, "shares" => shares, "symbol" => symbol} = conn.body_params

    case Token.verify_and_validate(token) do
      {:ok, claims} ->
        %{"user_id" => user_id} = claims

        {:ok, %HTTPoison.Response{status_code: status_code, body: body}} =
          HTTPoison.get(
            "https://sandbox.tradier.com/v1/markets/quotes?symbols=#{symbol}",
            @header
          )

        case status_code do
          401 ->
            conn
            |> send_resp(404, Poison.encode!(%{Response: "Failed. Incorrect API KEY!"}))

          _ ->
            decoded_body = Jason.decode!(body)

            %{
              "quotes" => %{
                "quote" => %{
                  "last" => last
                }
              }
            } = decoded_body

            {shares, _} = Integer.parse(shares)

            Repo.insert(%Logs{
              type: "sell",
              user_id: user_id,
              shares: shares,
              quoted_price: last,
              symbol: symbol
            })

            Repo.insert(%Logs{
              type: "buy",
              user_id: 0,
              shares: shares,
              quoted_price: last,
              symbol: symbol
            })

            total_cost = shares * last

            Repo.insert(%Ledger{
              type: "buy",
              shares: shares,
              quoted_price: last,
              total_cost: total_cost,
              symbol: symbol
            })

            conn
            |> send_resp(
              200,
              Jason.encode!(%{user_id: user_id, shares: shares, quoted_price: last})
            )
        end

      {:error, reason} ->
        conn
        |> send_resp(
          404,
          Poison.encode!(%{Response: "Failed. Incorrect user token KEY! #{reason}"})
        )
    end
  end

  match _ do
    conn
    |> send_resp(200, Jason.encode!(%{Response: "Not a valid route"}))
  end
end
