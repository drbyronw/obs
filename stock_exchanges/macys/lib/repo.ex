defmodule Repo do
  use Ecto.Repo,
    otp_app: :macys,
    adapter: Ecto.Adapters.Postgres
end
