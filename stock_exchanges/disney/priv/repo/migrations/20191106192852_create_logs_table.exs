defmodule Repo.Migrations.CreateLogsTable do
  use Ecto.Migration

  def change do
    create table (:logs) do
      add :type, :string
      add :user_id, :integer
      add :shares, :integer
      add :quoted_price, :float

      timestamps()
    end
  end
end
