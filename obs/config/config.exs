# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :obs,
  ecto_repos: [Obs.Repo]

config :obs,
  auth_endpoint: System.get_env("AUTH_ENDPOINT") || "http://localhost:4005",
  disney_endpoint: System.get_env("DISNEY_ENDPOINT") || "http://localhost:4000",
  facebook_endpoint: System.get_env("FACEBOOK_ENDPOINT") || "http://localhost:4001",
  heinz_endpoint: System.get_env("HEINZ_ENDPOINT") || "http://localhost:4002",
  lyft_endpoint: System.get_env("LYFT_ENDPOINT") || "http://localhost:4003",
  macys_endpoint: System.get_env("LYFT_ENDPOINT") || "http://localhost:4004"

# Configures the endpoint
config :obs, ObsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "avznL6UAujnh0/pVt22NdLK5nZwXWXMnq0XVvO0KlO2c3aa7L+vprsLpwT8SWjH/",
  render_errors: [view: ObsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Obs.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :joken, default_signer: "secret"
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
