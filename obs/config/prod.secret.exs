# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
use Mix.Config

database_url = System.get_env("DATABASE_URL") || "postgres://postgres:password@localhost:5432/"

# config :obs, Obs.Repo,
#   # ssl: true,
#   url: database_url,
#   pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

config :obs, Obs.Repo,
  username: "postgres",
  password: "password",
  database: "obs_prod",
  socket_dir:
    System.get_env("SOCKET_DIR") || "/tmp/cloudsql/sonic-column-261820:us-east1:obsdv-prod-2",
  pool_size: 15

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    "IIeav8y8bhYCGKYHjwWNfHTYISP8hRHRUjs0U23u9xxX5hG2A4DD8NhVFCOxGr8R"

config :obs, ObsWeb.Endpoint,
  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],
  secret_key_base: secret_key_base

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :obs, ObsWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
