defmodule Obs.TransactionContext do
  alias Obs.Transaction
  alias Obs.Repo
  alias Obs.Account
  alias Obs.AccountContext
  alias Obs.Stock
  import Ecto.Query, only: [from: 2]

  def add_funds(amount, account_id, user_id) when amount > 0 do
    fields = [user_id: user_id, money_received: amount, shares: 0, stock_ticker: "add_funds"]
    acct = Repo.get(Account, account_id)

    case acct do
      nil ->
        {:error, "account not found"}

      acct ->
        if user_id == acct.user_id do
          changeset = Ecto.build_assoc(acct, :transactions, fields)
          Repo.insert(changeset)

          {:ok, "added funds"}
        else
          {:error, "account does not correspond with user"}
        end
    end
  end

  def add_funds(_, _, _) do
    {:error, "must be nonzero amount"}
  end

  def buy(ticker, amount, account_id, user_id, token) when amount > 0 do
    current_price = Stock.get_price(ticker, token)

    if {:ok, price} = current_price do
      money_to_user = -(amount * price)

      acct = Repo.get(Account, account_id)

      case acct do
        nil ->
          {:error, "account not found"}

        acct ->
          acct = acct |> Repo.preload(:transactions)

          available_funds = AccountContext.total_balance(acct)

          if user_id == acct.user_id and available_funds + money_to_user > 0 do
            acct = acct |> Repo.preload(:transactions)

            case Stock.purchase_stock(ticker, amount, token) do
              {:ok, %{shares: bought_shares, price: bought_price}} ->
                fields = [
                  user_id: user_id,
                  amount: amount,
                  user_id: user_id,
                  money_received: -(bought_shares * bought_price),
                  stock_ticker: ticker,
                  shares: bought_shares
                ]

                changeset = Ecto.build_assoc(acct, :transactions, fields)
                Repo.insert(changeset)

                {:ok, "bought stocks"}

              {:error, reason} ->
                {:error, reason}
            end
          else
            {:error, "not authorized or insufficient funds"}
          end
      end
    else
      {:error, "stock service unavailable"}
    end
  end

  def sell(ticker, amount, account_id, user_id, token) when amount > 0 do
    current_price = Stock.get_price(ticker, token)

    case current_price do
      {:ok, price} ->
        acct = Repo.get(Account, account_id)
        IO.puts("\n\n\n hey")
        IO.inspect(amount)

        case acct do
          nil ->
            {:error, "account not found"}

          acct ->
            acct = acct |> Repo.preload(:transactions)

            available_stocks = total_holdings(acct, ticker)

            if user_id == acct.user_id and available_stocks >= amount do
              acct = acct |> Repo.preload(:transactions)

              case Stock.purchase_stock(ticker, amount, token) do
                {:ok, %{shares: sold_shares, price: sold_price}} ->
                  fields = [
                    user_id: user_id,
                    money_received: sold_shares * sold_price,
                    stock_ticker: ticker,
                    shares: -sold_shares
                  ]

                  changeset = Ecto.build_assoc(acct, :transactions, fields)
                  Repo.insert(changeset)

                  {:ok, "bought stocks"}

                {:error, reason} ->
                  {:error, reason}
              end
            else
              {:error, "not authorized or insufficient number of stocks"}
            end
        end

      {:error, reason} ->
        {:error, reason}
    end
  end

  def buy(_, _, _, _, _) do
    {:error, "must be >0 number of shares"}
  end

  def sell(_, _, _, _, _) do
    {:error, "must be >0 number of shares"}
  end

  defp total_funds(account) do
  end

  defp total_holdings(account, stock) do
    case Repo.one(
           from t in Transaction,
             where: [account_id: ^account.id, stock_ticker: ^stock],
             select: sum(t.shares)
         ) do
      nil -> 0
      holdings -> holdings
    end
  end
end
