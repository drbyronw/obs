defmodule Obs.Transaction do
  use Ecto.Schema

  import Ecto.Changeset

  schema "transactions" do
    belongs_to :account, Obs.Account
    field :user_id, :integer
    field :money_received, :float
    field :shares, :integer
    field :stock_ticker, :string
  end

  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:user_id, :money_received, :shares, :stock_ticker, :account_id])
  end
end
