defmodule Obs.AccountContext do
  alias Obs.Account
  alias Obs.Repo
  require Ecto.Query

  def get_accounts_for_user(user_id) do
    Account
    |> Ecto.Query.where(user_id: ^user_id)
    |> Repo.all()
    |> Repo.preload(:transactions)
  end

  def create(user_id, name) do
    %Account{}
    |> Account.changeset(%{name: name, user_id: user_id})
    |> Repo.insert()
  end

  def total_balance(%{transactions: tx}) do
    tx
    |> Enum.reduce(0, fn tx, acc -> tx.money_received + acc end)
  end

  def owned_shares(%{transactions: tx}, ticker) do
    tx
    |> Enum.reduce(0, fn tx, acc ->
      if tx.stock_ticker == ticker do
        tx.shares + acc
      else
        acc
      end
    end)
  end
end
