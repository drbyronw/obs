defmodule ObsWeb.AccountController do
  use ObsWeb, :controller
  alias Obs.AccountContext, as: Account

  plug ObsWeb.Plug.Authorize

  def create_index(conn, _params) do
    render(conn, "create.html")
  end

  def create(conn, %{"name" => name}) do
    case Account.create(conn.assigns[:user_id], name) do
      {:ok, acct} ->
        conn
        |> put_flash(:info, "account created")
        |> redirect(to: "/")

      {:error, changeset} ->
        conn
        |> put_flash(:error, changeset.errors[0])
    end
  end
end
