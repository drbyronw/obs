defmodule ObsWeb.AuthController do
  use ObsWeb, :controller
  alias Obs.Auth

  def login_index(conn, _) do
    case get_session(conn, "token") do
      nil -> render(conn, "login.html")
      token -> redirect(conn, to: "/")
    end
  end

  def register_index(conn, _) do
    case get_session(conn, "token") do
      nil -> render(conn, "register.html")
      token -> redirect(conn, to: "/")
    end
  end

  def login(conn, %{"username" => username, "password" => password}) do
    case Auth.login(username, password) do
      {:ok, token} ->
        conn
        |> put_session("token", token)
        |> redirect(to: "/")

      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: "/login")
    end
  end

  def register(conn, %{"username" => username, "password" => password}) do
    case Auth.register(username, password) do
      {:ok, token} ->
        conn
        |> put_session("token", token)
        |> redirect(to: "/")

      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: "/register")
    end
  end

  def logout(conn, _) do
    conn
    |> delete_session("token")
    |> put_flash(:info, "logged out")
    |> redirect(to: "/login")
  end
end
