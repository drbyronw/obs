defmodule ObsWeb.HomeView do
  use ObsWeb, :view

  alias Obs.AccountContext, as: Account

  def total_balance(acct) do
    Account.total_balance(acct)
  end

  def owned_shares(acct, ticker) do
    IO.inspect(Account.owned_shares(acct, ticker))
  end
end
