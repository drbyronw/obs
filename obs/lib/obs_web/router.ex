defmodule ObsWeb.Router do
  use ObsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
    plug :put_user_id
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ObsWeb do
    pipe_through :browser

    get "/", HomeController, :index

    get "/login", AuthController, :login_index
    get "/logout", AuthController, :logout
    get "/register", AuthController, :register_index
    post "/login", AuthController, :login
    post "/register", AuthController, :register

    get "/accounts/create", AccountController, :create_index
    post "/accounts/create", AccountController, :create

    post "/transactions/add_funds", TransactionController, :add_funds
    post "/transaction", TransactionController, :create
  end

  @doc """
  This is a simple plug that just shoves the username/user_id of a logged in user into the conn struct.
  """
  defp put_user_id(conn, _) do
    case get_session(conn, "token") do
      nil ->
        conn

      token ->
        IO.inspect(token)

        case Obs.Token.verify_and_validate(token) do
          {:ok, claims} ->
            conn
            |> assign(:user_id, claims["user_id"])
            |> assign(:username, claims["username"])

          {:error, _} ->
            conn
        end
    end
  end
end
