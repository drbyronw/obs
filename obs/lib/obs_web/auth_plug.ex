defmodule ObsWeb.Plug.Authorize do
  import Plug.Conn

  def init(default), do: default

  def call(conn, _default) do
    case conn.assigns[:user_id] do
      nil ->
        conn
        |> halt
        |> Phoenix.Controller.redirect(to: "/login")

      _ ->
        conn
    end
  end
end
