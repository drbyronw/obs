defmodule Obs.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :account_id, references(:accounts)
      add :user_id, :int
      add :money_received, :float
      add :shares, :int
      add :stock_ticker, :string
    end
  end
end
