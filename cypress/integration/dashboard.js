let username = "usernameTest"
let password = "longerthan8ok"

let badUsername = "user"
let badPassword = "short"

// **Scenario**: Authenticated user Bob is registered for OBS and wants to view his dashboard
// - **Given**: Bob has already registered for an OBS account
// - **When**: Bob signs into his account using his authenticated credentials in the homepage sign-in
// - **Then**: Bob will see his dashboard and options to make transactions
describe("User has already registered for an OBS account", function()    {
    it("User signs into his account using his authenticated credentials in the homepage sign-in", function()    {
        cy.launchOBS()
        cy.register("dash1", password)
        cy.contains("user: dash1")
        cy.contains("BUY/SELL")
    })
})


// **Scenario**: Un-authenticated user Bob is trying to engage in stock transactions
//   - **Given**: Bob has not registered for an official OBS account
//   - **When**: Bob opens the OBS homepage and inputs necessary information to make a stock transaction
//   - **Then**: Bob is given an error message and he cannot make any stock transactions
describe("User has not registered for an official OBS account", function()    {
    it("User opens the OBS homepage and inputs necessary information to make a stock transaction", function()    {
        cy.launchOBS()
        cy.login("dash2", password)
        cy.contains("user not found or incorrect password")
    })
})

// Opposite
describe("User has registered for an official OBS account", function()    {
    it("User opens the OBS homepage and inputs necessary information to make a stock transaction", function()    {
        cy.launchOBS()
        cy.register("dash3", password)
        cy.contains("ticker")
        cy.contains("account name")
        cy.contains("how many?")
    })
})


// **Scenario**: OBS dashboard displays current stock prices for Bank Inc stocks with client portfolio information.
//   - **Given**: Bob has an OBS account
//   - **When**: Bob opens the “Dashboard” page in a web browser
//   - **Then**: Bob sees all five current stock prices AND their portfolio information (current # of shares of all five stocks and amount of money in account)
// Un-authenticated user Bob is trying to engage in stock transactions
describe("User has an OBS account", function()    {
    it("User opens the OBS homepage and inputs necessary information to make a stock transaction", function()    {
        cy.launchOBS()
        cy.register("dash4", password)
        cy.contains("user: dash4")
        cy.contains("BUY/SELL")

        cy.contains('#fb', /[0-9\.]+/).should('exist')
        cy.contains('#m', /[0-9\.]+/).should('exist')
        cy.contains('#khc', /[0-9\.]+/).should('exist')
        cy.contains('#dis', /[0-9\.]+/).should('exist')
        cy.contains('#lyft', /[0-9\.]+/).should('exist')
    })
})
