let username = "usernameTest"
let password = "longerthan8ok"

let badUsername = "user"
let badPassword = "short"

// **Scenario**: Logged in user Bob wants to purchase or sell shares
//   - **Given**: Bob is authorized AND has shares or funds to use for transactions
//   - **When**: Bob opens the buy/sell page in a web browser AND enters the amount of his desired transaction (either how many shares to sell or how many to buy) AND has adequate assets (either enough shares to sell the desired amount or enough money to purchase desired number of shares) AND submits the buy/sell form
//   - **Then**: Bob sees a transaction confirmation AND Bobâ€™s assets have changed accordingly (either acquiring new shares and losing funds or acquiring more funds and losing shares)
describe("User is authorized AND has shares or funds to use for transactions", function()    {
    it("User opens the buy page in a web browser AND enters the amount of his desired transaction (how many to buy) AND has adequate assets (enough money to purchase desired number of shares) AND submits the buy form", function()    {
        cy.launchOBS()
        cy.register("trans1", password)
        cy.contains("BUY/SELL")

        cy.createAccount("blue")
        
        cy.addFunds(1, 500)
        cy.contains("successfully added funds")

        cy.buy("FB", "blue", 1)
        cy.contains("successfully purchased")
    })
})

describe("User is authorized AND has shares or funds to use for transactions", function()    {
    it("User opens the sell page in a web browser AND enters the amount of his desired transaction (how many shares to sell) AND has adequate assets (either enough shares to sell the desired amount) AND submits the sell form", function()    {
        cy.launchOBS()
        cy.register("trans2", password)
        cy.contains("BUY/SELL")

        cy.createAccount("blue")
        
        cy.addFunds(1, 500)
        cy.contains("successfully added funds")

        cy.buy("FB", "blue", 1)
        cy.contains("successfully purchased")

        cy.sell("FB", "blue", 1)
        cy.contains("successfully sold")
    })
})

// **Scenario**: Authed user cannot purchase shares at a cost greater than the cash held in the account
//   - **Given**: Bob has logged in AND attempts to purchase more shares than he can currently afford
//   - **When**: Bob opens the â€œTransactionâ€ page in a web browser AND properly enters to buy an amount of shares with cost greater than held in the account
//   - **Then**: Bob sees a transaction failed response AND redirected back to the â€œTransactionâ€ page
describe("User has logged in AND attempts to purchase more shares than he can currently afford", function()    {
    it("User opens the Transaction page in a web browser AND properly enters to buy an amount of shares with cost greater than held in the account", function()    {
        cy.launchOBS()
        cy.register("trans3", password)
        cy.contains("BUY/SELL")

        cy.createAccount("blue")
        cy.addFunds(1, 500)
        cy.contains("successfully added funds")

        cy.buy("FB", "blue", 5)
        cy.contains("not authorized or insufficient funds")
    })
})

describe("User has logged in AND attempts to purchase more shares than he can currently afford", function()    {
    it("User opens the Transaction page in a web browser AND properly enters to sell more shares than currently has", function()    {
        cy.launchOBS()
        cy.register("trans4", password)
        cy.contains("BUY/SELL")

        cy.createAccount("blue")
        cy.addFunds(1, 500)
        cy.contains("successfully added funds")

        cy.sell("FB", "blue", 5)
        cy.contains("not authorized or insufficient number of stocks")
    })
})

// **Scenario**: Bob wants to see his holdings (stock and funds) per account
//   - **Given**: Bob is an authorized user AND already has multiple OBS accounts registered AND has purchased stocks or added funds to more than one account
//   - **When**: Bob opens the dashboard page in a web browser AND looks at a list of his assets
//   - **Then**: Bob sees his stock and cash holdings divided between his various accounts corresponding to which account was used to purchase/add funds
describe("User is an authorized user AND already has multiple OBS accounts registered AND has purchased stocks or added funds to more than one account", function()    {
    it("User opens the dashboard page in a web browser AND looks at a list of his assets", function()    {
        cy.launchOBS()
        cy.register("trans5", password)
        cy.contains("BUY/SELL")

        cy.createAccount("green")
        cy.createAccount("red")
        cy.createAccount("blue")
        cy.addFunds(1, 5000)
        cy.contains("successfully added funds")
        cy.addFunds(2, 5000)
        cy.contains("successfully added funds")
        cy.addFunds(3, 5000)
        cy.contains("successfully added funds")

        cy.buy("FB", "blue", 5)
        cy.contains("successfully purchased")
        cy.buy("DIS", "red", 5)
        cy.contains("successfully purchased")
        cy.buy("LYFT", "red", 5)
        cy.contains("successfully purchased")
        cy.buy("KHC", "green", 15)
        cy.contains("successfully purchased")
        cy.buy("M", "green", 4)
        cy.contains("successfully purchased")

        cy.sell("FB", "blue", 1)
        cy.contains("successfully sold")
        cy.sell("DIS", "red", 1)
        cy.contains("successfully sold")
        cy.sell("LYFT", "red", 1)
        cy.contains("successfully sold")
        cy.sell("KHC", "green", 1)
        cy.contains("successfully sold")
        cy.sell("M", "green", 1)
        cy.contains("successfully sold")

        cy.get("#stock_exchanges>tbody").contains('tr', "blue").children('td').eq(1).should('contain', "4")
        cy.get("#stock_exchanges>tbody").contains('tr', "red").children('td').eq(4).should('contain', "4")
        cy.get("#stock_exchanges>tbody").contains('tr', "red").children('td').eq(5).should('contain', "4")
        cy.get("#stock_exchanges>tbody").contains('tr', "green").children('td').eq(3).should('contain', "14")
        cy.get("#stock_exchanges>tbody").contains('tr', "green").children('td').eq(2).should('contain', "3")
    })
})
