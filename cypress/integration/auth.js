let username = "usernameTest"
let password = "longerthan8ok"

let badUsername = "user"
let badPassword = "short"

// **Scenario**: New client Bob wants to register for OBS
//   - **Given**: Bob is new and doesn’t have an OBS account
//   - **When**: Bob opens the OBS homepage and he inputs the necessary data to sign-up
//   - **Then**: Bob will see a registration confirmation and he’s then logged into to the system where he sees the dashboard first and can make transactions whenever
describe("User is new and doesn't have an OBS account", function()    {
    it("Opens the OBS homepage and inputs the neccessary data to sign-up", function()    {
        cy.launchOBS()
        cy.register(username, password)
        cy.contains("user: usernameTest")
    })
})

// Opposite
describe("User is new and doesn't have an OBS account", function()    {
    it("Opens the OBS homepage and inputs invalid data to sign-up", function()    {
        cy.launchOBS()
        cy.register(badUsername, badPassword)
        cy.contains("password: should be at least 8 character(s)")
    })
})

// **Scenario**: Clients must input valid username/password combination or given an error message when logging in.
//   - **Given**: Bob has registered for access AND has an OBS account
//   - **When**: Bob opens the “Login” page in a web browser AND incorrectly enters the required fields
//   - **Then**: Bob sees a login failed response AND is not logged in to the system - redirected to “Login” page
describe("User has registered for access AND has an OBS account", function()    {
    it("User opens the 'Login' page in a web browser AND incorrectly enters the required fields", function()    {
        cy.launchOBS()
        cy.visit("http://localhost:4006/login")
        cy.login(badUsername, password)
        cy.contains("user not found or incorrect password")
    })
})

// Opposite
describe("User has registered for access AND has an OBS account", function()    {
    it("User opens the 'Login' page in a web browser AND corretly enters the required fields", function()    {
        cy.launchOBS()
        cy.visit("http://localhost:4006/login")
        cy.login(username, password)
        cy.contains("user: usernameTest")
    })
})
