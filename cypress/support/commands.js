// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
let dashboardURL = "http://localhost:4006"
Cypress.Commands.add("launchOBS", () => { 
    cy.visit(dashboardURL)
 })

Cypress.Commands.add("register", (username, password) => { 
    cy.get('a').contains("register").click()
    cy.login(username, password)
 })

Cypress.Commands.add("login", (username, password) => { 
    cy.get('#username')
          .type(username)
          .should('have.value', username)
        
    cy.get('#password')
        .type(password)
        .should('have.value', password)
    
    cy.get('form').submit()
 })

 Cypress.Commands.add("logout", () => { 
    cy.get('a').contains("logout").click()
 })

Cypress.Commands.add("createAccount", (name) => { 
    cy.get('a').contains("create new account").click()
    cy.get('input[name="name"]').type(name)
    cy.get('form').submit()
})

Cypress.Commands.add("addFunds", (index, amount) => { 
    cy.get('form[action="/transactions/add_funds"]>input').eq((index-1)*3).type(amount)
    cy.get('form[action="/transactions/add_funds"]>input').eq(((index-1)*3)+2).click()
})

Cypress.Commands.add("buy", (ticker, account, amount) => { 
    cy.get('select[name="ticker"]').select(ticker)
    cy.get('select[name="account_id"]').select(account)
    cy.get('input[value="buy"]').check()
    cy.get('input[placeholder="# shares"]').type(amount)
    cy.get('input[value="submit"]').click()
})

Cypress.Commands.add("sell", (ticker, account, amount) => { 
    cy.get('select[name="ticker"]').select(ticker)
    cy.get('select[name="account_id"]').select(account)
    cy.get('input[value="sell"]').check()
    cy.get('input[placeholder="# shares"]').type(amount)
    cy.get('input[value="submit"]').click()
})
